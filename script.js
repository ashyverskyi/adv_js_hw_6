document.addEventListener("DOMContentLoaded", function() {
    const findIpButton = document.getElementById("findIpButton");
    const locationInfo = document.getElementById("locationInfo");

    findIpButton.addEventListener("click", async function() {
        try {
            const clientIpResponse = await fetch("https://api.ipify.org/?format=json");
            const clientIpData = await clientIpResponse.json();
            const clientIp = clientIpData.ip;

            const locationResponse = await fetch(`http://ip-api.com/json/${clientIp}`);
            const locationData = await locationResponse.json();

            console.log("Server Response:", locationData);

            const infoHTML = `
                <p>Country: ${locationData.country}</p>
                <p>Country Code: ${locationData.countryCode}</p>
                <p>Region: ${locationData.regionName} (${locationData.region})</p>
                <p>City: ${locationData.city}</p>
                <p>Zip Code: ${locationData.zip}</p>
                <p>Latitude: ${locationData.lat}</p>
                <p>Longitude: ${locationData.lon}</p>
                <p>Timezone: ${locationData.timezone}</p>
                <p>ISP: ${locationData.isp}</p>
                <p>Organization: ${locationData.org}</p>
                <p>AS: ${locationData.as}</p>
                <p>Query: ${locationData.query}</p>
            `;
            
            locationInfo.innerHTML = infoHTML;
        } catch (error) {
            console.error("Error:", error);
            locationInfo.innerHTML = "<p>Error retrieving location information.</p>";
        }
    });
});








